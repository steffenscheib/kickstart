#!/bin/bash

# read only global variables
declare -r __PRESEED_TEMPLATE="/root/preseed_templates/preseed.template"
declare -r __PRESEED_POST_INSTALL_TEMPLATE="/root/preseed_templates/preseed_post_install.template"
declare -r __PUBLIC_DIRECTORY="/var/www/html/preseed"
declare -r __INITRD_PATH="/var/lib/vz/template/kernel/preseed/initrd.gz"
declare -r __KERNEL_PATH="/var/lib/vz/template/kernel/preseed/linux"
declare -r __DHCPD_CONFIGURATION_FILE="/etc/dhcp/dhcpd.conf"
declare -r __DHCPD_LEASE_FILE="/var/lib/dhcp/dhcpd.leases"

# VM variables 
declare -i __VM_ID=-1
declare -i __VM_MEMORY=-1
declare -i __VM_CORES=-1
declare -i __VM_DISK_SIZE=-1
declare __VM_INTERFACE_NAME=""
declare __VM_IP=""
declare __VM_HOSTNAME=""
declare __VM_DOMAIN_NAME=""


# create opt string
declare __OPT_STRING="-o hi:n:d:m:c:f:s:p: --long help,id:,hostname:,domain:,memory:,cores:,interface:,disk:,ip:"
declare __ARGUMENTS="$(getopt "${__OPT_STRING}" -- "${@}")"

function vm_create::is_id_available () {
  ( [[ -n "${1}" ]] &&
    [[ "${1}" =~ ^[[:digit:]]+$ ]]
  ) || {
    echo "ERROR: ${FUNCNAME}: First argument is not an integer";
    return 1;
  };
  declare -i vmId="${1}"

  while read -r vmLine; do
    [[ "${vmLine}" =~ ^[[:digit:]]+$ ]] || {
      continue;
    };

    [[ "${vmId}" =~ ^${vmLine}$ ]] || {
      continue;
    };

    # found a match -> vm ID not available
    return 1;
  done < <(qm list | awk '{ print $1 }' )

  # no match found -> vm ID available
  return 0;
} #; function vm_create::is_id_available ( <vmId> )


function vm_create::is_vm_running () {
  ( [[ -n "${1}" ]] &&
    [[ "${1}" =~ ^[[:digit:]]+$ ]]
  ) || {
    echo "ERROR: ${FUNCNAME}: First argument is not an integer";
    return 1;
  };
  declare -i vmId="${1}"

  while read -r vmLine; do
    # skip the header line
    [[ "${vmLine}" =~ ^[[:digit:]] ]] || {
      continue;
    };

    # read the line into an array
    IFS=" " read -ra vmInformation <<<"${vmLine}"
    declare id="${vmInformation[0]}"
    declare name="${vmInformation[1]}"
    declare status="${vmInformation[2]}"
    declare pid="${vmInformation[5]}"

    [[  "${vmId}" =~ ^${id}$ ]] || {
      # current VM is not the VM we are looking for
      continue;
    };

    echo "Name: ${name} ID: '${id}' PID: '${pid}' Status: '${status}'"
    [[ ! "${status}" =~ ^running$ ]] || {
      # vm still running
      echo "INFO: VM still running";
      return 0;
    };

    # vm not running anymore
    return 1;
  done < <(qm list)

  # no match found -> vm ID available
  return 0;
} #; function vm_create::is_id_available ( <vmId> )

function vm_create::set_static_dhcp_ip () {
  [[ -n "${1}" ]] || {
    echo "ERROR: ${FUNCNAME} did not recieve the first argument!"
    return 1;
  };
  declare hostname="${1}"

  [[ -n "${2}" ]] || {
    echo "ERROR: ${FUNCNAME} did not recieve the second argument!"
    return 2;
  };
  declare ip="${2}"

  [[ -n "${3}" ]] || {
    echo "ERROR: ${FUNCNAME} did not recieve the third argument!"
    return 3;
  };
  declare mac="${3}"

  # find out, whether the last line is }
  [[ "$(awk '/./{line=$0} END{print line}' "${__DHCPD_CONFIGURATION_FILE}")" =~ \} ]] || {
    echo "ERROR: The last line of the DHCPD configuration file ('${__DHCPD_CONFIGURATION_FILE}') is not '}'!";
    return 4;
  };

  # remove the last line of the dhcpd file
  sed -i '$ d' "${__DHCPD_CONFIGURATION_FILE}" || {
    echo "ERROR: Failed to remove the last line of the DHCPD configuration file ('${__DHCPD_CONFIGURATION_FILE}')!";
    return 5;
  }; 


# paste the block to the dhcp configuration file
cat >> "${__DHCPD_CONFIGURATION_FILE}" << EOF

   host ${hostname} {
     hardware ethernet ${mac};
     fixed-address ${ip};
   }
}
EOF

# restart the dhcpd to pick up the changes
systemctl restart isc-dhcp-server || {
  echo "ERROR: Restarting the DHCPD failed!";
  return 4;
};

# everything went fine
return 0;
} #; function vm_create::set_static_dhcp_ip ( <hostname> <ip> <mac )

function vm_create::get_mac_address () {
  ( [[ -n "${1}" ]] &&
    [[ "${1}" =~ ^[[:digit:]]+$ ]]
  ) || {
    echo "ERROR: ${FUNCNAME} did not recieve the first argument or it is not a valid integer (assigned: '${1}')!";
    return 1;
  };
  declare -i vmId="${1}"

  __VM_MAC="$(qm config "${vmId}" | grep net0 | awk '{print $2}' | sed -e 's@virtio=@@' -e 's@,.*@@')"
  return 0;
} #; function vm_create::get_mac_address ( <vmId> )

function vm_create::is_ip_available () {
  [[ -n "${1}" ]] || {
    echo "ERROR: ${FUNCNAME} did not recieve the first argument!";
    return 1;
  };
  declare ip="${1}"

  ! grep "${ip}" "${__DHCPD_CONFIGURATION_FILE}" &> /dev/null || {
    echo "ERROR: IP '${ip}' already assigned in '${__DHCPD_CONFIGURATION_FILE}'!";
    return 2;
  };

  ! grep "${ip}" "${__DHCPD_LEASE_FILE}" &> /dev/null || {
    echo "ERROR: IP '${ip}' already found in lease file ('${__DHCPD_LEASE_FILE}')!";
    return 3;
  };

  # ip not found
  return 0;
} #; function vm_create::is_ip_available ( <ip> )

function print_help () {
  echo "Following switches are available:"
  echo "  -h/--help                  : Print this screen"
  echo "  -i/--id                    : ID for the VM"
  echo "  -n/--hostname <hostname>   : Set the desired hostname for the system to install"
  echo "  -d/--domain <domain>       : Set the desired domain for the system to install"
  echo "  -m/--memory <memory>       : Memory in megabytes for the virtual machine"
  echo "  -c/--cores <cores>         : Number of cores to use for the virtual machine"
  echo "  -f/--interface <interface> : The name of the interface from the guest system to use"
  echo "  -s/--disk <size>           : Size in gigabytes for the disk of the system"
  echo "  -p/--ip <ip>               : IP to assign via DHCP"
} #; print_help

# parse command line arguments
eval set -- "${__ARGUMENTS}"
while [[ -n "${1}" ]]; do
  case "${1}" in
    -h|--help)
      print_help
      exit 0;
    ;;
    -i|--id)
      # check if the ID is not empty and an integer
      ( [[ "${2}" =~ ^[[:digit:]]+$ ]] &&
        [[ -n "${2}" ]]
      ) || {
        printf "ERROR: Given ID ('${2}') is either empty or not an integer!\n";
        exit 1;
      };

      __VM_ID="${2}"
      shift 2
    ;;
    -m|--memory)
      # check if the given value is not empty and an integer
      ( [[ "${2}" =~ ^[[:digit:]]+$ ]] &&
        [[ -n "${2}" ]]
      ) || {
        printf "ERROR: Given value to use as memory ('${2}') is either empty or not an integer!\n";
        exit 1;
      };

      __VM_MEMORY="${2}"
      shift 2
    ;;
    -s|--disk)
      # check if the given value is not empty and an integer
      ( [[ "${2}" =~ ^[[:digit:]]+$ ]] &&
        [[ -n "${2}" ]]
      ) || {
        printf "ERROR: Given value to use a disk size ('${2}') is either empty or not an integer!\n";
        exit 1;
      };

      __VM_DISK_SIZE="${2}"
      shift 2
    ;;
    -c|--cores)
      # check if the given value is not empty and an integer
      ( [[ "${2}" =~ ^[[:digit:]]+$ ]] &&
        [[ -n "${2}" ]]
      ) || {
        printf "ERROR: Given value to use as number of cores ('${2}') is either empty or not an integer!\n";
        exit 1;
      };

      __VM_CORES="${2}"
      shift 2
    ;;
    -f|--interface)
      # check if the interface is not empty
      [[ -n "${2}" ]] || {
        print "ERROR: The given interface ('${2}') is empty!\n";
        exit 1;
      };

      __VM_INTERFACE_NAME="${2}"
      shift 2
    ;;
    -d|--domain)
      # check if the domain is not empty
      [[ -n "${2}" ]] || {
        print "ERROR: The given domain ('${2}') is empty!\n";
        exit 1;
      };

      __VM_DOMAIN_NAME="${2}"
      shift 2
    ;;
    -n|--hostname)
      # check if the hostname is not empty
      [[ -n "${2}" ]] || {
        print "ERROR: The given hostname ('${2}') is empty!\n";
        exit 1;
      };

      __VM_HOSTNAME="${2}"
      shift 2
    ;;
    -p|--ip)
      # check if the ip is not empty
      [[ -n "${2}" ]] || {
        print "ERROR: The given IP ('${2}') is empty!\n";
        exit 1;
      };

      __VM_IP="${2}"
      shift 2
    ;;
    --)
      # this argument (usually) marks the end of the arguments
      shift
    ;;
    *)
      printf "Invalid argument '${1}' given, ignoring.\n";
      shift
    ;;
  esac
done

vm_create::is_id_available "${__VM_ID}" || {
  echo "ERROR: VM ID '${__VM_ID}' already in use!";
  exit 1;
};

# copy templates to destination public directory
cp "${__PRESEED_TEMPLATE}" "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.preseed"
cp "${__PRESEED_POST_INSTALL_TEMPLATE}" "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.late"

# replace hostname in the preseed files
sed "s@<HOSTNAME>@"${__VM_HOSTNAME}"@g" -i "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.preseed" -i "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.late"

# replace domain in the preseed files
sed "s@<DOMAIN>@"${__VM_DOMAIN_NAME}"@g" -i "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.preseed" -i "${__PUBLIC_DIRECTORY}/${__VM_HOSTNAME}.late"

# create the VM
qm create "${__VM_ID}" --virtio0 "local:${__VM_DISK_SIZE}" --net0 "virtio,bridge=${__VM_INTERFACE_NAME}" --ostype l26 --memory "${__VM_MEMORY}" --cores "${__VM_CORES}" --name "${__VM_HOSTNAME}.${__VM_DOMAIN_NAME}" --args "-kernel ${__KERNEL_PATH} -initrd ${__INITRD_PATH} -append \"preseed/url=https://http.scheib.me/${__VM_HOSTNAME}.preseed debian-installer/allow_unauthenticated_ssl=true netcfg/enable=true netcfg/choose_interface=auto auto=true ipv6.disable=1 netcfg/get_hostname=${__VM_HOSTNAME} netcfg/get_domain=${__VM_DOMAIN_NAME}\"" || {
  echo "The qm create command failed!";
  exit 1;
};

[[ -z "${__VM_IP}" ]] || {
  # when an ip is given we set the static configuration for the vm
  vm_create::get_mac_address "${__VM_ID}" || {
    echo "ERROR: Getting the MAC address of VM with ID '${__VM_ID}' failed!";
    exit 1;
  };

  vm_create::is_ip_available "${__VM_IP}" || {
    exit 1;
  };

  vm_create::set_static_dhcp_ip "${__VM_HOSTNAME}.${__VM_DOMAIN_NAME}" "${__VM_IP}" "${__VM_MAC}" || {
    echo "ERROR: Creating static entry for the VM '${__VM_HOSTNAME}.${__VM_DOMAIN_NAME}' with IP '${__VM_IP}' and MAC '${__VM_MAC}'!";
    exit 1;
  };
};

# start the VM
qm start "${__VM_ID}" || {
  echo "The qm start command failed!";
  exit 1;
};

while true; do
  vm_create::is_vm_running "${__VM_ID}"
  returnCode="${?}"

  [[ "${returnCode}" -ne 0 ]] || {
    sleep 30;
    continue;
  };

  # vm is stopped
  break; 
done

qm set "${__VM_ID}" -delete args || {
  echo "The qm set command failed!";
  exit 1;
};

qm start "${__VM_ID}" || {
  echo "The qm start command failed!";
  exit 1;
};
